package com.baydar.ktsa;


/*
 * PlaceRank class
 * An information for ranking places
 */
public class PlaceRank implements Comparable<PlaceRank>{

	String id;
	double rankPoint;

	public PlaceRank(String id, double rankPoint) {
		this.id = id;
		this.rankPoint = rankPoint;
	}
	
	//Compares two PlaceRanks according to their rankPoints
	public int compareTo(PlaceRank o) {
		if (o.rankPoint < this.rankPoint) {
			return -1;
		} else if (o.rankPoint > this.rankPoint) {
			return 1;
		} else {
			return 0;
		}
	}


}
