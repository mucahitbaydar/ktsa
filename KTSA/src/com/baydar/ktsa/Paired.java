package com.baydar.ktsa;


/*
 * Paired class
 * Keeps information for tuples
 * Stores distances with ids
 */
public class Paired implements Comparable<Paired> {

	String id;
	double distance;

	public Paired(String id, double distance) {
		this.id = id;
		this.distance = distance;
	}

	public int compareTo(Paired o) {
		if (o.distance > this.distance) {
			return -1;
		} else if (o.distance < this.distance) {
			return 1;
		} else {
			return 0;
		}
	}

}
