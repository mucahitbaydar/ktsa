# KTSA(Location Prediction in Location-Based Social Networks)

This project is developed for testing the new proposed method for location 
prediction in location-based social networks and comparing with other methods.


## Dependencies

There are two external libraries used in the project. First one is 
Joda time library. It is used for date conversion issues. Second one
is postgresql jdbc library. It is used for db connection process.


## Philosophy

Data used in the project is not going to share because of copyright 
issues. 
This project is my Masters' thesis. I proposed a new method for 
location prediction in location-based social networks. There is also
a paper available about my workings, so, I'm not going to explain 
every bit of detail here. 
Paper is available through the following link:
https://sproc.org/ojs/index.php/gjit/article/view/2835

